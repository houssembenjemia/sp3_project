


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>S3P</title>

		<style>
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2) {
				text-align: right;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:last-child {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		</style>
	</head>

	<body>
		<div class="invoice-box">
			<img src="{{ env('APP_URL').('/logo.png') }}" style="width: auto; height: 50px"></br>

			<table cellpadding="0" cellspacing="0">
				<tr class="top">
					<td colspan="6">
						<table>
							<tr>
								<td >

									Plexiglass -P.V.C - Lamica - Aluminium composite(Alucobond)<br>
									Plaques d'immatriculation et Accessoires Vinyle et bache en bobine</br>
									Stores à bras invisible - Afficheurs et écrans LED
								</td>
								
								<td>
									STE S3P DISTRIBUTION TUNISIE</br>
									21 CITE ENNOUR - ARIANA 2080 TUNIS</br>

TEL 70 740 245 / FAX: 70 740 244</br>

MF : 1388238 CBE 001 / RC: B9133472015</br>

CODE EN DOUANE : 1388238C</br>

RIB 25 005 000 0000 069990 10 - ZITOUNA
								</td>
							</tr>
							
						</table>
					</td>
				</tr>


				<tr class="top">
					<td colspan="6">
						<table>
							<tr>
								<td >
									Sahloul Le {{date("Y/m/d")}}<br>
									<h4>Facture Proforma N° {{$invoice->no_devis}}</h4> 
								</td>

								<td>
								<b>Client {{$invoice->client->num_client}} </b><br>

								<b>Nom : {{$invoice->client->nom_social}} </br>
								<b>Adresse : {{$invoice->client->adresse}} </br>
								<b>M F : {{$invoice->client->num_id_fiscale}} </b>

								</td>
							</tr>
							
						</table>
					</td>
				</tr>


				
				<tr class="heading">
					<td>Réf</td>
					<td>Désignation</td>

					<td>Qté</td>
					<td>PUHT</td>
					<td>TVA%</td>
					<td>Rem%</td>
					<td>PTHT</td>

				</tr>

                @foreach ($invoice->Devisarticles as $art)
                <tr class="item">
					<td>{{$art->ref}}</td>
					<td>{{$art->design}}</td>
					<td>{{$art->pivot->nbProduct}}</td>
					<td>{{$art->pv_ttc}}</td>
					<td>{{$art->tva->taux}}</td>
					<td>{{$art->remise}}</td>
					<td>{{number_format($art->pv_ttc * $art->pivot->nbProduct, 3, '.', "")}}</td>


				</tr>
                @endforeach
				

			

				<tr class="total">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>

						
					<td >Total: {{$invoice->montant_tht}}</br>
						Remise: {{$invoice->mnt_remise}}</br>
						Net HT: {{$invoice->montant_tht}}</br>
						Total TVA: {{$invoice->total_tva}}</br>
						Timbre: {{$invoice->timbre}}</br>
						Total TTC: {{$invoice->montant_tht}}</br>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>