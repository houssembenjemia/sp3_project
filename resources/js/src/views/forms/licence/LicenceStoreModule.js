import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {

        fetchPays() {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/get/pays`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },

    fetchLicenses(ctx, queryParams) {
            return new Promise((resolve, reject) => {
              axios
                .get('/get/licenses', { params: queryParams })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          save(ctx, userData,data) {
            return new Promise((resolve, reject) => {
              axios
                .post('/forms/license/change_status', { data: userData,status:data })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          fetchLicence(ctx, { id }) {
            return new Promise((resolve, reject) => {
              axios
                .get(`/forms/license/licenses/${id}`)
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },

    },//actions
}
