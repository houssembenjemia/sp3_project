import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {

        fetchVisas(ctx, queryParams) {
            return new Promise((resolve, reject) => {
              axios
                .get('/get/visas', { params: queryParams })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          save(ctx, userData,data) {
            return new Promise((resolve, reject) => {
              axios
                .post('/forms/visa/change_status', { data: userData,status:data })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          fix_rdv(ctx, userData,data) {
            return new Promise((resolve, reject) => {
              axios
                .post('/forms/visa/fix_rdv', { data: userData,status:data })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          fetchVisa(ctx, { id }) {
            return new Promise((resolve, reject) => {
              axios
                .get(`/forms/visa/visas/${id}`)
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },

        fetchPays() {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/get/pays`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchBranches(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/get/branches/`, {
                        ctx
                    })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        createTraveller(ctx, userData) {
            return new Promise((resolve, reject) => {
                axios
                    .post(`/traveller_store`, {
                        data: userData,
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        //PAYMENT
        hyperpaypayment(ctx, {
            brand,
            cart,
            amount,
            user_id
        }) {
            return new Promise((resolve, reject) => {
                axios
                    .post('hyperpay/payment', {
                        brand: brand,
                        cart: cart,
                        amount: amount,
                        user_id: user_id
                    }, {
                        headers: {
                            'ek-current-fqdn': 'main.ekliel.net'
                        }
                    })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },

        //PAYMENT
    },
}
