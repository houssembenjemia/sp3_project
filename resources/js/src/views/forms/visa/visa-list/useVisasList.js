import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { maritalStatus } from '@core/utils/filter'

// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function useVisasList() {
  // Use toast
  const toast = useToast()

  const refUserListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', label:'#', sortable: true },
    {
      key: 'fName',
      label: 'First Name',
      //formatter: title,
      sortable: true,
    },
    {
      key: 'lName',
      label: 'Last Name',
      //formatter: title,
      sortable: true,
    },
    {
      key: 'users.email',
      label: 'Email',
      //formatter: title,
      sortable: true,
    },
      {
        key: 'visas',
        label: 'Visa Type',
        //formatter: title,
        sortable: true,
      },
      {
        key: 'country',
        label: 'Country',
        //formatter: title,
        sortable: true,
      },
      {
        key: 'branch',
        label: 'Visa Centers',
        sortable: true,
      },

      {
        key: 'etat_civil',
        label: 'Marital Status',
        // formatter: maritalStatus,
        sortable: true,
      },
   // { key: 'email', sortable: true },
    /*{ key: 'role', sortable: true },
    {
      key: 'currentPlan',
      label: 'Plan',
      formatter: title,
      sortable: true,
    },*/
    {
        key: 'status', //sortable: true
    },
    {
        key: 'rdv',
        label: 'rdv',
        sortable: false,
      },
    { key: 'actions' },
  ]
  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('created_at')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
//app-user/fetchVisas
  const fetchVisas = (ctx, callback) => {
    store
      .dispatch('forms-visa/fetchVisas', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        //role: roleFilter.value,
        //plan: planFilter.value,
        //status: statusFilter.value,
      })
      .then(response => {
        //console.log('tt',response)
        const totalUsers = response.data.meta.total
        const users = response.data.data
        console.log('tt',response)
        callback(users)
        totalUsers.value = total

      })
      .catch(() => {
        /*toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })*/
      })
  }

  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  const resolveUserRoleVariant = role => {
    if (role === 'subscriber') return 'primary'
    if (role === 'author') return 'warning'
    if (role === 'maintainer') return 'success'
    if (role === 'editor') return 'info'
    if (role === 'admin') return 'danger'
    return 'primary'
  }

  const resolveUserRoleIcon = role => {
    if (role === 'subscriber') return 'UserIcon'
    if (role === 'author') return 'SettingsIcon'
    if (role === 'maintainer') return 'DatabaseIcon'
    if (role === 'editor') return 'Edit2Icon'
    if (role === 'admin') return 'ServerIcon'
    return 'UserIcon'
  }

  const resolveUserStatusVariant = status => {
    if (status === 'pending') return 'warning'
    if (status === 'active') return 'success'
    if (status === 'inactive') return 'secondary'
    if (status === 'cancel') return 'danger'
    if (status === 'approved') return 'success'
    return 'primary'
  }

  return {
    fetchVisas,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,
    resolveUserRoleVariant,
    resolveUserRoleIcon,
    resolveUserStatusVariant,
    refetchData,
    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
