import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchPays() {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/get/pays`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },

        fetchTranslations(ctx, queryParams) {
            return new Promise((resolve, reject) => {
              axios
                .get('/get/translations', { params: queryParams })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },

          save(ctx, userData,data) {
            return new Promise((resolve, reject) => {
              axios
                .post('/forms/translation/change_status', { data: userData,status:data })
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
          fetchTranslation(ctx, { id }) {
            return new Promise((resolve, reject) => {
              axios
                .get(`/forms/translation/translations/${id}`)
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
          },
    },//actions
}
