import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchClients(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/clients', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchClient(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/client/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addClient(ctx, clientData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/client/add', { client: clientData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        delete(ctx, clientData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/client/delete', { client: clientData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateClient(ctx, clientData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/client/update', { client: clientData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
