import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { title } from '@core/utils/filter'
// houssem
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function useFournisseursList() {
    // Use toast
    const toast = useToast()

    const refFournisseurListTable = ref(null)

    // Table Handlers
    const tableColumns = [
        { key: 'id', sortable: true },
        { key: 'type', sortable: true },
        { key: 'numero', sortable: true },
        { key: 'nom_social', sortable: true },
        { key: 'matricule_fiscal', sortable: true },
        { key: 'email', sortable: true },
        { key: 'num_tel', sortable: true },
        { key: 'adresse', sortable: true },
        // { key: 'role', sortable: true },
        // {
        //   key: 'currentPlan',
        //   label: 'Plan',
        //   formatter: title,
        //   sortable: true,
        // },
        { key: 'actions' },
    ]
    const perPage = ref(10)
    const totalFournisseurs = ref(0)
    const currentPage = ref(1)
    const perPageOptions = [10, 25, 50, 100]
    const searchQuery = ref('')
    const sortBy = ref('id')
    const isSortDirDesc = ref(true)
    const roleFilter = ref(null)
//   const planFilter = ref(null)
    const statusFilter = ref(null)

    const dataMeta = computed(() => {
        const localItemsCount = refFournisseurListTable.value ? refFournisseurListTable.value.localItems.length : 0
        return {
            from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
            to: perPage.value * (currentPage.value - 1) + localItemsCount,
            of: totalFournisseurs.value,
        }
    })

    const refetchData = () => {
        refFournisseurListTable.value.refresh()
    }

    watch([currentPage, perPage, searchQuery, roleFilter, /*planFilter,*/ statusFilter], () => {
        refetchData()
    })

    const fetchFournisseurs = (ctx, callback) => {
        store
            .dispatch('app-fournisseur/fetchFournisseurs', {
                q: searchQuery.value,
                perPage: perPage.value,
                page: currentPage.value,
                sortBy: sortBy.value,
                sortDesc: isSortDirDesc.value,
                // plan: planFilter.value,
                status: statusFilter.value,
            })
            .then(response => {
                const fournisseurs = response.data.data
                const total = response.data.meta.total
                callback(fournisseurs)
                totalFournisseurs.value = total
            })
            .catch(() => {
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching Fournisseurs list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger',
                    },
                })
            })
    }

    // *===============================================---*
    // *--------- UI ---------------------------------------*
    // *===============================================---*





    return {
        fetchFournisseurs,
        tableColumns,
        perPage,
        currentPage,
        totalFournisseurs,
        dataMeta,
        perPageOptions,
        searchQuery,
        sortBy,
        isSortDirDesc,
        refFournisseurListTable,

        refetchData,

    }
}
