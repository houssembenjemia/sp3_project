import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchFournisseurs(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/fournisseurs', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchFournisseur(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/fournisseur/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addFournisseur(ctx, fournisseurData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/fournisseur/add', { fournisseur: fournisseurData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        delete(ctx, fournisseurData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/fournisseur/delete', { fournisseur: fournisseurData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateFournisseur(ctx, fournisseurData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/fournisseur/update', { user: fournisseurData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
