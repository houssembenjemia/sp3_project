import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchInvoices(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/bonCommande/list', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

      last_invoice_id() {
      return new Promise((resolve, reject) => {
        axios
          .get('/bonCommande/last_bonCommande_id')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchInvoice(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/bonCommande/bonCommande/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchClients() {
      return new Promise((resolve, reject) => {
        axios
          .get('/list/client')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    printInvoice(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/bonCommande/bonCommande/print/${id}`,{
            responseType: 'arraybuffer'
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchLivreurs() {
      return new Promise((resolve, reject) => {
        axios
          .get('/get/livreurs')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchItems() {
      return new Promise((resolve, reject) => {
        axios
          .get('/all/articles')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addClient(ctx, clientData) {
      return new Promise((resolve, reject) => {
          axios
              .post('/client/add', { client: clientData })
              .then(response => resolve(response))
              .catch(error => reject(error))
      })
  },
  addDevisClient(ctx, data,array) {
    return new Promise((resolve, reject) => {
      axios
        .post('/add/bonCommande', { data: data,array:array })
        .then(response => resolve(response))
        .catch(error => reject(error))
    })
},
    // addUser(ctx, userData) {
    //   return new Promise((resolve, reject) => {
    //     axios
    //       .post('/apps/user/users', { user: userData })
    //       .then(response => resolve(response))
    //       .catch(error => reject(error))
    //   })
    // },
  },
}
