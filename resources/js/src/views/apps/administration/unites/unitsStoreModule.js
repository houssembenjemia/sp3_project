import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchUnits(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/units', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchUnit(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/unit/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addUnit(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/unit/add', { unit: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        delete(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/unit/delete', { unit: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateUnit(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/unit/update', { unit: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
