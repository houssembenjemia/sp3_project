import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchTvas(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/tvas', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchTva(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/tva/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addTva(ctx, tvaData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/tva/add', { tva: tvaData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        delete(ctx, tvaData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/tva/delete', { tva: tvaData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateTva(ctx, tvaData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/tva/update', { tva: tvaData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
