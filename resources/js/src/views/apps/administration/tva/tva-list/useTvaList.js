import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { title } from '@core/utils/filter'
// houssem
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function useTvasList() {
    // Use toast
    const toast = useToast()

    const refTvaListTable = ref(null)

    // Table Handlers
    const tableColumns = [
        { key: 'id', sortable: true },
        { key: 'name', sortable: true },
        { key: 'taux', sortable: true },
        // { key: 'role', sortable: true },
        // {
        //   key: 'currentPlan',
        //   label: 'Plan',
        //   formatter: title,
        //   sortable: true,
        // },
        { key: 'actions' },
    ]
    const perPage = ref(10)
    const totalUsers = ref(0)
    const currentPage = ref(1)
    const perPageOptions = [10, 25, 50, 100]
    const searchQuery = ref('')
    const sortBy = ref('id')
    const isSortDirDesc = ref(true)
//   const planFilter = ref(null)
    const statusFilter = ref(null)

    const dataMeta = computed(() => {
        const localItemsCount = refTvaListTable.value ? refTvaListTable.value.localItems.length : 0
        return {
            from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
            to: perPage.value * (currentPage.value - 1) + localItemsCount,
            of: totalUsers.value,
        }
    })

    const refetchData = () => {
        refTvaListTable.value.refresh()
    }

    watch([currentPage, perPage, searchQuery, /*planFilter,*/ statusFilter], () => {
        refetchData()
    })

    const fetchTvas = (ctx, callback) => {
        store
            .dispatch('app-tva/fetchTvas', {
                q: searchQuery.value,
                perPage: perPage.value,
                page: currentPage.value,
                sortBy: sortBy.value,
                sortDesc: isSortDirDesc.value,
                // plan: planFilter.value,
                status: statusFilter.value,
            })
            .then(response => {
                const users = response.data.data
                const total = response.data.meta.total

                callback(users)
                totalUsers.value = total
            })
            .catch(() => {
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching Tvas list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger',
                    },
                })
            })
    }

    // *===============================================---*
    // *--------- UI ---------------------------------------*
    // *===============================================---*





    return {
        fetchTvas,
        tableColumns,
        perPage,
        currentPage,
        totalUsers,
        dataMeta,
        perPageOptions,
        searchQuery,
        sortBy,
        isSortDirDesc,
        refTvaListTable,

        refetchData,

    }
}
