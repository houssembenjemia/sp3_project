import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchCurrencies(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/currencies', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchCurrencie(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/currencie/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addCurrencie(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/currencie/add', { currencie: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        delete(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/currencie/delete', { currencie: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateCurrencie(ctx, unitData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/currencie/update', { currencie: unitData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
