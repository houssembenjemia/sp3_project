import axios from '@axios'

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchMarques(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/marques', { params: queryParams })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchMarque(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/marque/get/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        // addMarque(ctx, marqueData) {
        //     return new Promise((resolve, reject) => {
        //         axios
        //             .post('/marque/add', { marque: marqueData })
        //             .then(response => resolve(response))
        //             .catch(error => reject(error))
        //     })
        // },
        delete(ctx, marqueData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/marque/delete', { marque: marqueData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        updateMarque(ctx, marqueData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/marque/update', { marque: marqueData })
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}
