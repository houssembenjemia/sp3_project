import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    addDevisClient(ctx, data,array) {
        return new Promise((resolve, reject) => {
          axios
            .post('/add/devisclient', { data: data,array:array })
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    fetchInvoices(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/apps/invoice/invoices', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchInvoice(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/apps/invoice/invoices/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchClients() {
      return new Promise((resolve, reject) => {
        axios
          .get('/list/client')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
      last_invoice_id() {
      return new Promise((resolve, reject) => {
        axios
          .get('/last_invoice_id')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchItems() {
        return new Promise((resolve, reject) => {
          axios
            .get('/all/articles')
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
      },
      addClient(ctx, clientData) {
        return new Promise((resolve, reject) => {
            axios
                .post('/client/add', { client: clientData })
                .then(response => resolve(response))
                .catch(error => reject(error))
        })
    },
    fetchLivreurs() {
      return new Promise((resolve, reject) => {
        axios
          .get('/get/livreurs')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    // addUser(ctx, userData) {
    //   return new Promise((resolve, reject) => {
    //     axios
    //       .post('/apps/user/users', { user: userData })
    //       .then(response => resolve(response))
    //       .catch(error => reject(error))
    //   })
    // },
  },
}
