import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchDevisClient(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/all/devisclient', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getListClient(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/client`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    getListDepot(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/depot`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    getListArticle(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/article`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    fetchArticleID(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/get/devisclient/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addDevisClient(ctx, data,array) {
      return new Promise((resolve, reject) => {
        axios
          .post('/add/devisclient', { data: data,array:array })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateDevisClient(ctx, data,array) {
        return new Promise((resolve, reject) => {
          axios
            .post('/update/devisclient', { data: data,array:array })
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
      },
    getAllProductDevisClient(ctx, ProductIdList) {
        return new Promise((resolve, reject) => {
          axios
            .post('/get/getAllProductDevisClient', { ProductIdList: ProductIdList })
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
      },
    delete(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/devisclient/delete', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/user/devisclient', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
