import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchFamilles(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/all/familles', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchFamilleID(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/get/familles/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/user/add', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/famille/delete', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/user/update', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
