import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { title } from '@core/utils/filter'
// houssem
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function useUsersList() {
  // Use toast
  const toast = useToast()

  const refUserListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', sortable: true },
    { key: 'ref', sortable: true },
    { key: 'code_bar', sortable: true },
    { key: 'qty_alert', sortable: true },
    { key: 'achat_ttc', sortable: true },
    // { key: 'design', sortable: true },
    // { key: 'libele_cataloge', sortable: true },
    // { key: 'id4', sortable: true },
    // { key: 'id3', sortable: true },
    // { key: 'id2', sortable: true },
    // { key: 'id1', sortable: true },
    // { key: 'tva', sortable: true },
    // { key: 'achat_ttc', sortable: true },
    // { key: 'prix_achat_ht', sortable: true },
    // { key: 'pv_ttc', sortable: true },
    // { key: 'marge', sortable: true },
    // { key: 'max_marge', sortable: true },
    // { key: 'matiere_active', sortable: true },
    // { key: 'remise', sortable: true },
    // { key: 'deb_remise', sortable: true },
    // { key: 'fin_remise', sortable: true },
    { key: 'actions' },
  ]
  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
//   const planFilter = ref(null)
  const statusFilter = ref(null)

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery, roleFilter, /*planFilter,*/ statusFilter], () => {
    refetchData()
  })

  const fetchArticle = (ctx, callback) => {
    store
      .dispatch('app-article/fetchArticle', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        // plan: planFilter.value,
        status: statusFilter.value,
      })
      .then(response => {
        const users = response.data.data
        const total = response.data.meta.total

        callback(users)
        totalUsers.value = total
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  const resolveUserRoleVariant = role => {
    // if (role === 'subscriber') return 'primary'
    // if (role === 'author') return 'warning'
    // if (role === 'maintainer') return 'success'
    if (role === 'user') return 'info'
    if (role === 'admin') return 'danger'
    return 'primary'
  }

  const resolveUserRoleIcon = role => {
    // if (role === 'subscriber') return 'UserIcon'
    // if (role === 'author') return 'SettingsIcon'
    // if (role === 'maintainer') return 'DatabaseIcon'
    if (role === 'user') return 'Edit2Icon'
    if (role === 'admin') return 'ServerIcon'
    return 'UserIcon'
  }

  const resolveUserStatusVariant = status => {
    // if (status === 'pending') return 'warning'
    if (status === 'active') return 'success'
    if (status === 'inactive') return 'secondary'
    return 'primary'
  }

  return {
    fetchArticle,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,

    resolveUserRoleVariant,
    resolveUserRoleIcon,
    resolveUserStatusVariant,
    refetchData,

    // Extra Filters
    roleFilter,
    // planFilter,
    statusFilter,
  }
}
