import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchArticle(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/all/articles', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getListFamille(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/familles`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    getListTva(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/tva`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    getListBrand(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/brand`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    getListUnite(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get(`/list/unite`)
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    fetchArticleID(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/get/article/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/user/add', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    uploadCsv(ctx, userData) {
        return new Promise((resolve, reject) => {
        axios
            .post('/upload/csv', { user: userData })
            .then(response => resolve(response))
            .catch(error => reject(error))
        })
    },
    delete(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/article/delete', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/user/update', { user: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
