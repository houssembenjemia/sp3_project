import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchDepots(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/all/depot', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchDepot(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/get/depot/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addDepot(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/add/depot', { depot: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/depot/delete', { depot: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateDepot(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/depot/update', { depot: userData })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
