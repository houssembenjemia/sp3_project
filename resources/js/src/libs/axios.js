import Vue from 'vue'

// axios
import axios from 'axios'

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: '/api',
  headers: {
    'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),
  }
})

Vue.prototype.$http = axiosIns

export default axiosIns
