export default [
    {
        header: 'Mouvements Clients',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'ListIcon',
                title: 'Liste des Paiements Clients',
                route: 'apps-list-paiements-list',
            },
            {
                icon: 'ListIcon',
                title: 'Liste Paiements Par Client',
                route: 'apps-list-paiement-client-list',
            },
            {
                icon: 'CodeIcon',
                title: 'Facture par Client',
                route: 'apps-facture-client-list',
            },
            {
                icon: 'BarChart2Icon',
                title: 'Etat Financier Global',
                route: 'apps-fianacier-global-list',
            },

            {
                icon: 'BookOpenIcon',
                title: 'Details BL/FA/BS',
                route: 'apps-bl-list',
            },
            {
                icon: 'AirplayIcon',
                title: 'Statistiques',
                route: 'apps-stats-list',
            },
            {
                icon: 'AnchorIcon',
                title: 'Intégration Paiements',
                route: 'apps-intgr-paiement-list',
            }
        ],
    },
]
