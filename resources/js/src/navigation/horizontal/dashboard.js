export default [
  {
    header: 'Dashboards',
    icon: 'HomeIcon',
    children: [
      {
        title: 'Ventes',
        route: 'dashboard-commerciale',
        icon: 'ShoppingCartIcon',
      },
      {
        title: 'Achats',
        route: 'dashboard-analytics',
        icon: 'ActivityIcon',
      },
    ],
  },
]
