export default [
    {
        header: 'Stock',
        icon: 'BoxIcon',
        children: [
            {
                icon: 'ArrowDownIcon',
                title: 'Bon d\'Entrée',
                route: 'apps-bonEntree-list',
            },
            {
                icon: 'ArrowUpIcon',
                title: 'Bon de Sortie',
                route: 'apps-bon-sorties-list',
            },
            {
                icon: 'ArrowRightIcon',
                title: 'Etat du Stock',
                route: 'apps-etat-stock-list',
            },
            {
                icon: 'CodesandboxIcon',
                title: 'Inventaire',
                route: 'apps-inventaire-list',
            },
            {
                icon: 'CornerDownRightIcon',
                title: 'Mouvements',
                route: 'apps-mouvement-list',
            },
            {
                icon: 'DatabaseIcon',
                title: 'Saisie Inventaire',
                route: 'apps-saisie-inventaire-list',
            }
        ],
    },
]
