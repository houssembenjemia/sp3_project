export default [
    {
        header: 'Articles',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'ArrowRightCircleIcon',
                title: 'Créer Article',
                route: 'apps-articles-list',
            },
            {
                icon: 'ArrowRightCircleIcon',
                title: 'Gérer Familles',
                route: 'apps-familles-list',
            },
            {
                icon: 'BriefcaseIcon',
                title: 'Famille',
                route: 'apps-famille-list',
            },
            {
                icon: 'ListIcon',
                title: 'Liste des Articles',
                route: 'apps-famille-list',
            },
            {
                icon: 'ArchiveIcon',
                title: 'Historique des Articles',
                route: 'apps-famille-list',
            },
            {
                icon: 'ArchiveIcon',
                title: 'Historique Famille',
                route: 'apps-famille-list',
            },
            {
                icon: 'ListIcon',
                title: 'Liste de Prix',
                route: 'apps-famille-list',
            },
            {
                icon: 'DollarSignIcon',
                title: 'Tarif Spécial',
                route: 'apps-famille-list',
            },
            {
                icon: 'TrendingDownIcon',
                title: 'Gestion Numéro de Série',
                route: 'apps-famille-list',
            }
        ],
    },
]
