export default [
    {
        header: 'Clients',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'ArrowRightCircleIcon',
                title: 'Création Client',
                route: 'apps-clients-list',
            },
            {
                icon: 'ListIcon',
                title: 'Liste des Clients',
                route: 'apps-clients-list',
            },
            {
                icon: 'BookmarkIcon',
                title: 'Devis/Facture Proforme',
                route: 'apps-devisclient-list',
            },
            {
                icon: 'ArrowRightIcon',
                title: 'Bons de Commande',
                route: 'apps-bon-commande-list',
            },
            {
                icon: 'ArrowUpIcon',
                title: 'Bons de Livraison',
                route: 'apps-bon-livraison-list',
            },
            {
                icon: 'CheckSquareIcon',
                title: 'Factures',
                route: 'apps-facture-list',
            },
            {
                icon: 'CheckSquareIcon',
                title: 'Avoirs',
                route: 'apps-avoirs-list',
            },
            {
                icon: 'CodeIcon',
                title: 'Transfert de Compte',
                route: 'apps-transfert-list',
            }
        ],
    },
]
