export default [
    {
        header: 'Journal',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'ActivityIcon',
                title: 'Journal des Factures Achats',
                route: 'apps-journal-achat-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Journal des Factures Ventes',
                route: 'apps-journal-factures-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Journal des BL Ventes',
                route: 'apps-journal-bl-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Journal des BL Achats',
                route: 'apps-journal-bl-achat-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Journal Caisse Vente',
                route: 'apps-journal-caisse-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Etat de Caisse',
                route: 'apps-etat-caisse-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Dépense Internes',
                route: 'apps-depense-interne-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Fond de Caisse',
                route: 'apps-fond-caisse-list',
            },
            {
                icon: 'ActivityIcon',
                title: 'Cloture Caisse',
                route: 'apps-cloture-caisse-list',
            }
        ],
    },
]
