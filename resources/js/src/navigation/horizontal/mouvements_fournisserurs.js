export default [
    {
        header: 'Mouvements Fournisseurs',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'DollarSignIcon',
                title: 'Paiements Fournisseurs',
                route: 'apps-paiement-f-list',
            },
            {
                icon: 'ChevronsDownIcon',
                title: 'Récap Paiements Par Fournisseur',
                route: 'apps-recap-list',
            },
            {
                icon: 'DatabaseIcon',
                title: 'Facture par Fournisseur',
                route: 'apps-facture-f-list',
            },
            {
                icon: 'DollarSignIcon',
                title: 'Etat Financier Global',
                route: 'apps-etat-f-list',
            },
            {
                icon: 'DollarSignIcon',
                title: 'Etat Financier Global des Fournisseurs',
                route: 'apps-etat-f-g-list',
            },
            {
                icon: 'Edit3Icon',
                title: 'Relevé Fournisseur',
                route: 'apps-relever-f-list',
            },
            {
                icon: 'BarChart2Icon',
                title: 'Statistiques',
                route: 'apps-stats-f-list',
            }
        ],
    },
]
