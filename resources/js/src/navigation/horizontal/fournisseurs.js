export default [
    {
        header: 'Fournisseurs',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'ArrowRightCircleIcon',
                title: 'Créer Fournisseur',
                route: 'apps-fournisseur-list',
            },
            {
                icon: 'ListIcon',
                title: 'Liste Des Fournisseurs',
                route: 'apps-fournisseur-list',
            },
            {
                icon: 'EditIcon',
                title: 'Demande Offre de Prix',
                route: 'apps-demande-prix-list',
            },
            {
                icon: 'Edit3Icon',
                title: 'Bons de Commande',
                route: 'apps-bon-commande-list',
            },
            {
                icon: 'Edit3Icon',
                title: 'Bons de Réception',
                route: 'apps-bon-reception-list',
            },
            {
                icon: 'DatabaseIcon',
                title: 'Factures',
                route: 'apps-factures-list',
            },
            {
                icon: 'DatabaseIcon',
                title: 'Avoirs',
                route: 'apps-avoirs-list',
            },
            {
                icon: 'CodeIcon',
                title: 'Transfert de Compte',
                route: 'apps-transfert-compte-list',
            },
            {
                icon: 'AlignJustifyIcon',
                title: 'Consultation Prix',
                route: 'apps-consult-prix-list',
            },
            {
                icon: 'ArchiveIcon',
                title: 'Historique Commande Fournisseur',
                route: 'apps-hist-commande-list',
            }
        ],
    },
]
