export default [
    {
        header: 'Banques',
        icon: 'PackageIcon',
        children: [
            {
                icon: 'BarChartIcon',
                title: 'Echéances de Paiements Client',
                route: 'apps-echeance-list',
            },
            {
                icon: 'BarChartIcon',
                title: 'Echéances de Paiements Fournisseurs',
                route: 'apps-echeance-paeiment-list',
            },
            {
                icon: 'CodeIcon',
                title: 'Situation Bancaire',
                route: 'apps-situation-list',
            },
            {
                icon: 'CornerRightUpIcon',
                title: 'Extra Dépense',
                route: 'apps-extra-depense-list',
            },
            {
                icon: 'CornerRightUpIcon',
                title: 'Sortie Coffre',
                route: 'apps-coffre-list',
            }
        ],
    },
]
