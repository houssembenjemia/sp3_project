export default [
    {
        header: 'Administration',
        icon: 'PackageIcon',
        children: [
            {
                title: 'Utilisateurs',
                icon: 'UserIcon',
                children: [
                    {
                        title: 'Liste des Utilisateurs',
                        route: 'apps-users-list',
                    }
                ],
            },
            {
                title: 'Marque',
                icon: 'UserIcon',
                route: 'apps-marque-list',
            },
            {
                title: 'Unité',
                icon: 'UserIcon',
                route: 'apps-units-list',
            },
            {
                title: 'Devise',
                icon: 'UserIcon',
                route: 'apps-devise-list',
            },
            {
                title: 'TVA',
                icon: 'UserIcon',
                route: 'apps-tva-list',
            },
            {
                title: 'Depot',
                icon: 'UserIcon',
                route: 'apps-depot-list',
            },
        ],
    },
]
