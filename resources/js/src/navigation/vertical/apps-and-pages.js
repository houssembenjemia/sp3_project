export default [
  {
    header: 'Administration',
  },
  {
    title: 'Users',
    icon: 'UserIcon',
    children: [
      {
        title: 'List',
        route: 'apps-users-list',
      }
    ],
  },
]
