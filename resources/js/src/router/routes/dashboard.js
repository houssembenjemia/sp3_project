export default [
  {
    path: '/dashboard/analytics',
    name: 'dashboard-analytics',
    component: () => import('@/views/dashboard/analytics/Analytics.vue'),
  },
  {
    path: '/dashboard/commerciale',
    name: 'dashboard-commerciale',
    component: () => import('@/views/dashboard/ecommerce/Ecommerce.vue'),
  },
]
