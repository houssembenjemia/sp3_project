export default [
    // *===============================================---*
    // *--------- articles ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/apps/bon-sortie/list',
        name: 'apps-bon-sorties-list',
        component: () => import('@/views/apps/bonSorties/bonSortie-list/bonSortieList.vue'),
    },


    // // *--------- devisClient ---- ---------------------------------------*
    // // *===============================================---*
    // {
    //     path: '/apps/devisclient/list',
    //     name: 'apps-devisclient-list',
    //     component: () => import('@/views/apps/devisClient/devisClient-list/devisClientList.vue'),
    // },
    // {
    //   path: '/apps/devisclient/edit/:id',
    //   name: 'apps-devisclient-edit',
    //   component: () => import('@/views/apps/devisclient/devisclient-edit/devisClientEdit.vue'),
    // }

]
