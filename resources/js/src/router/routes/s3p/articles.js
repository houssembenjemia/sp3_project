export default [
    // *===============================================---*
    // *--------- articles ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/apps/articles/list',
      name: 'apps-articles-list',
      component: () => import('@/views/apps/articles/articles-list/ArticlesList.vue'),
    },
    {
        path: '/apps/familles/list',
        name: 'apps-familles-list',
        component: () => import('@/views/apps/familles/familles-list/FamillesList.vue'),
      },
    // {
    //   path: '/apps/users/view/:id',
    //   name: 'apps-users-view',
    //   component: () => import('@/views/apps/user/users-view/UsersView.vue'),
    // },
    {
      path: '/apps/articles/edit/:id',
      name: 'apps-articles-edit',
      component: () => import('@/views/apps/articles/articles-edit/ArticlesEdit.vue'),
    },
    {
        path: '/apps/familles/edit/:id',
        name: 'apps-familles-edit',
        component: () => import('@/views/apps/familles/familles-edit/FamillesEdit.vue'),
    },


  ]
