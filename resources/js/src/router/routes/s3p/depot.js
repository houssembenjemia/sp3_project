export default [
    // *===============================================---*
    // *--------- depot ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/apps/depot/list',
        name: 'apps-depot-list',
        component: () => import('@/views/apps/depot/depot-list/DepotList.vue'),
    },
    {
      path: '/apps/depot/edit/:id',
      name: 'apps-depot-edit',
      component: () => import('@/views/apps/depot/depot-edit/DepotEdit.vue'),
    },

]
