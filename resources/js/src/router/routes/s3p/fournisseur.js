export default [
    // *===============================================---*
    // *--------- articles ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/apps/fournisseur/list',
        name: 'apps-fournisseur-list',
        component: () => import('@/views/apps/fournisseurs/fournisseurs-list/FournisseurList.vue'),
    },
    {
      path: '/apps/fournisseur/bonCommand/list',
      name: 'apps-bon-commande-list',
      component: () => import('@/views/apps/fournisseurs/bonCommand/invoice-list/InvoiceList.vue'),
    },

    {
      path: '/apps/fournisseur/bonCommand/preview/:id',
      name: 'apps-bon-commande-preview',
      component: () => import('@/views/apps/fournisseurs/bonCommand/invoice-preview/InvoicePreview.vue'),
    },
    // {
    //   path: '/apps/users/edit/:id',
    //   name: 'apps-users-edit',
    //   component: () => import('@/views/apps/user/users-edit/UsersEdit.vue'),
    // },

    {
      path: '/apps/fournisseur/bonCommand/add',
      name: 'apps-bon-commande-add',
      component: () => import('@/views/apps/fournisseurs/bonCommand/invoice-add/InvoiceAdd.vue'),
    },

]
