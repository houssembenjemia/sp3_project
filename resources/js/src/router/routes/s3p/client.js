export default [
    // *===============================================---*
    // *--------- articles ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/apps/clients/list',
        name: 'apps-clients-list',
        component: () => import('@/views/apps/clients/clients-list/ClientList.vue'),
    },


    // *--------- devisClient ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/apps/devisclient/list',
      name: 'apps-devisclient-list',
      component: () => import('@/views/apps/devisClient/devisClient-list/devisClientList.vue'),
    },
    {
      path: '/apps/devisclient/edit/:id',
      name: 'apps-devisclient-edit',
      component: () => import('@/views/apps/devisClient/devisClient-edit/devisClientEdit.vue'),
    },
    {
        path: '/apps/devisclient/client/add',
        name: 'apps-devisclient-list',
        component: () => import('@/views/apps/devisClient/invoice-add/InvoiceAdd.vue'),
      },

]
