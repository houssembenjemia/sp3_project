<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api','LocaleMiddleware']], function($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('/register', 'AuthController@register');


    Route::group(['middleware' => 'jwt.auth', 'providers' => 'jwt', 'refresh.token'], function() {
        Route::get('/profile', 'AuthController@profile');

        Route::get('/users', 'UserController@index');
        Route::post('/user/add', 'UserController@store');
        Route::post('/user/delete', 'UserController@destroy');
        Route::get('/user/get/{id}','UserController@getUser');
        Route::post('/user/update','UserController@update');
        Route::post('me', 'AuthController@me');


        //Marques Routes
        Route::get('/marques', 'BrandController@index');
        Route::post('/marque/delete', 'BrandController@delete');
        Route::post('/marque/add', 'BrandController@store');
        Route::get('/list/brand', 'BrandController@getListBrand');


        //Units Routes
        Route::get('/units', 'UnitController@index');
        Route::post('/unit/delete', 'UnitController@delete');
        Route::post('/unit/add', 'UnitController@store');
        Route::get('/list/unite', 'UnitController@getListUnite');


        //Currencies Routes
        Route::get('/currencies', 'CurrencieController@index');
        Route::post('/currencie/delete', 'CurrencieController@delete');
        Route::post('/currencie/add', 'CurrencieController@store');

        //Tva Routes
        Route::get('/tvas', 'TvaController@index');
        Route::post('/tva/delete', 'TvaController@delete');
        Route::post('/tva/add', 'TvaController@store');
        Route::get('/list/tva', 'TvaController@getListTva');


        //article
        Route::get('/all/articles', 'ArticleController@index');
        Route::post('/add/articles', 'ArticleController@store');
        Route::post('/update/articles', 'ArticleController@update');
        Route::post('/article/delete', 'ArticleController@destroy');
        Route::get('/get/article/{id}', 'ArticleController@show');
        Route::get('/list/article', 'ArticleController@getListArticle');
        Route::post('/upload/csv', 'ArticleController@uploadCsv');


        //famille
        Route::get('/all/familles', 'FamilleController@index');
        Route::get('/list/familles', 'FamilleController@getListFamille');
        Route::post('/add/familles', 'FamilleController@store');
        Route::post('/famille/delete', 'FamilleController@destroy');
        Route::get('/get/familles/{id}', 'FamilleController@show');
        Route::post('/update/familles', 'FamilleController@update');

        //Tva Fournisseur
        Route::get('/fournisseurs', 'FournisseurController@index');
        Route::post('/fournisseur/delete', 'FournisseurController@delete');
        Route::post('fournisseur/add', 'FournisseurController@store');

        //client
        Route::get('/list/client', 'DevisController@getListClient');
        Route::get('/last_invoice_id', 'DevisController@last_invoice_id');
        Route::get('/invoice/invoices/{id}', 'DevisController@getInvoiceById');
        Route::get('/invoice/invoices/print/{id}', 'DevisController@printInvoice');


        Route::get('/clients', 'ClientController@index');
        Route::post('/client/delete', 'ClientController@delete');
        Route::post('/client/add', 'ClientController@store');
        //depot
        Route::get('/all/depot', 'DepotController@index');
        Route::get('/list/depot', 'DepotController@getListDepot');
        Route::post('/add/depot', 'DepotController@store');
        Route::post('/depot/delete', 'DepotController@destroy');
        Route::post('/depot/update', 'DepotController@update');
        Route::get('/get/depot/{id}', 'DepotController@show');


        //Devis Client
        Route::get('/all/devisclient', 'DevisController@index');
        Route::post('/add/devisclient', 'DevisController@store');
        Route::post('/update/devisclient', 'DevisController@update');
        Route::post('/devisclient/delete', 'DevisController@destroy');
        Route::get('/get/devisclient/{id}', 'DevisController@show');
        Route::post('/get/getAllProductDevisClient', 'DevisController@getAllProductDevisClient');
        Route::get('/get/livreurs', 'DevisController@getAllLivreurs');


        //Bon Commande
        Route::get('/bonCommande/list', 'BonCommandController@index');
        Route::get('/bonCommande/last_bonCommande_id', 'BonCommandController@last_bonCommande_id');
        Route::get('/bonCommande/bonCommande/{id}', 'BonCommandController@getInvoiceById');
        Route::get('/bonCommande/bonCommande/print/{id}', 'BonCommandController@printInvoice');
        Route::post('/add/bonCommande', 'BonCommandController@store');

    });

});
