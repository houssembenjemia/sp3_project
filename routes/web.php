<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\DevisController;
use App\Models\Devis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/devis/devis/{id}', [DevisController::class, 'getDevistPdf']);
Route::get('/invoice/invoices/print/{id}',  function ($id) {
    $invoice = Devis::find($id);
        
        return view('devisTemplate', compact('invoice'));
});


Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '.*');
