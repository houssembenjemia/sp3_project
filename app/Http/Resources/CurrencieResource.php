<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'code' => $this->code,
            'name'=>$this->name,
            'valeur_echange'=>$this->valeur_echange,
            'id'=>$this->id,

        ];
    }
}
