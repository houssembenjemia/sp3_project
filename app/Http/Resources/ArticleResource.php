<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'famille_id'=>$this->famille_id,
            'design'=>$this->design,
            'code_bar'=>$this->code_bar,
            'ref'=>$this->ref,
            'libele_cataloge'=>$this->libele_cataloge,
            'id4'=>$this->id4,
            'id3'=>$this->id3,
            'id2'=>$this->id2,
            'id1'=>$this->id1,
            'tva_id'=>$this->tva_id,
            'tva'=>new TvaResource($this->tva)??'',
            'achat_ttc'=>$this->achat_ttc,
            'prix_achat_ht'=>$this->prix_achat_ht,
            'pv_ttc'=>$this->pv_ttc,
            'prix_vente'=>$this->prix_vente,
            'marge'=>$this->marge,
            'brand_id'=>$this->brand_id,
            'max_marge'=>$this->max_marge,
            'matiere_active'=>$this->matiere_active,
            'unit_id'=>$this->unit_id,
            'qty_alert'=>$this->qty_alert,
            'remise'=>$this->remise,
            'deb_remise'=>$this->deb_remise,
            'fin_remise'=>$this->fin_remise,
            'nbProduct'=>$this->pivot?(int)$this->pivot->nbProduct:1,
            'files' => FilesArticleResource::collection($this->files)
        ];
    }
}
