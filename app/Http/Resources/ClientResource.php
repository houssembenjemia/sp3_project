<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'num_client' => $this->num_client,
            'nom_social' => $this->nom_social,
            'adresse' => $this->adresse,
            'num_tel' => $this->num_tel,
            'num_fax' => $this->num_fax,
            'email' => $this->email,
            'contact' => $this->contact,
            'num_portable' => $this->num_portable,
            'num_id_fiscale' => $this->num_id_fiscale,
            'exonorer' => $this->exonorer,
            'timbre' => $this->timbre,
            'remise' => $this->remise,
            'mnt_garanti' => $this->mnt_garanti,
            'piece_garanti' => $this->piece_garanti,
            'activiter' => $this->activiter,
            'zone' => $this->zone,
            'categorie' => $this->categorie,
            'num_chef_groupe' => $this->num_chef_groupe,
            'libelle_chef_groupe' => $this->libelle_chef_groupe,
        ];
    }
}
