<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UnitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'code' => $this->code,
            'unite_de_base'=>$this->unite_de_base,
            'title'=>$this->title,
            'id'=>$this->id,

        ];
    }
}
