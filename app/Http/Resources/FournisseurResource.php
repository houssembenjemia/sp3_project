<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FournisseurResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'type'=>$this->type,
            'numero'=>$this->numero,
            'image'=>$this->image,
            'nom_social'=>$this->nom_social,
            'adresse'=>$this->adresse,
            'num_tel'=>$this->num_tel,
            'email'=>$this->email,
            'num_fax'=>$this->num_fax,
            'contact'=>$this->contact,
            'matricule_fiscal'=>$this->matricule_fiscal,
            'remise'=>$this->remise,
            'id'=>$this->id,

        ];
    }
}
