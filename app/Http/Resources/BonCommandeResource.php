<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BonCommandeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'client_id'=>$this->client->nom_social,
            'depot_id'=>$this->depot_id,
            'no_devis'=>$this->no_devis,
            'montant_tht'=>$this->montant_tht,
            'status'=>$this->status,
            'net_ht'=>$this->net_ht,
            'total_tva'=>$this->total_tva,
            'timbre'=>$this->timbre,
            'montant_ttc'=>$this->montant_ttc,
            'Devisarticles'=>ArticleResource::collection($this->Devisarticles)
        ];
    }
}
