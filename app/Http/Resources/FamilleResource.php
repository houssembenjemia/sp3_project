<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FamilleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'code_famille'=>$this->code_famille,
            'design'=>$this->design,
            'image'=>$this->image,
            'famille_parentale'=>$this->famille_parentale,
            'default_tva'=>$this->default_tva,
            'default_marge'=>$this->default_marge
        ];
    }
}
