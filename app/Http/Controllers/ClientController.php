<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClientResource;
use App\Http\Resources\UnitResource;
use App\Models\Client;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Client::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('num_client', 'LIKE', '%' . $keyword . '%');
            $userQuery->orWhere('nom_social', 'LIKE', '%' . $keyword . '%');
        }

        return ClientResource::collection($userQuery->paginate($limit));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $tva = Client::create([
            'num_client' => $params['client']['num_client'],
            'nom_social' => $params['client']['nom_social'],
            'adresse' => $params['client']['adresse'],
            'num_tel' => $params['client']['num_tel'],
            'num_fax' => $params['client']['num_fax'],
            'email' => $params['client']['email'],
            'contact' => $params['client']['contact'],
            'num_portable' => $params['client']['num_portable'],
            'num_id_fiscale' => $params['client']['num_id_fiscale'],
            'exonorer' => $params['client']['exonorer'],
            'timbre' => $params['client']['timbre'],
            'remise' => $params['client']['remise'],
            'mnt_garanti' => $params['client']['mnt_garanti'],
            'piece_garanti' => $params['client']['piece_garanti'],
            'activiter' => $params['client']['activiter'],
            'zone' => $params['client']['zone'],
            'categorie' => $params['client']['categorie'],
            'num_chef_groupe' => $params['client']['num_chef_groupe'],
            'libelle_chef_groupe' => $params['client']['libelle_chef_groupe'],

        ]);
        return response()->json(['data' => $tva, 'code' => 200]);

    }

    public function delete(Request $request)
    {
        try {
            $user=Client::where('id', $request->unit)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }
}
