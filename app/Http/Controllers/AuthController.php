<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\LoginResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use JWTAuth;
use App\Traits\ResponseTrait;
use App\Http\Requests\Auth\AuthPostRequest;
use App\Traits\RespondsWithHttpStatus;
use DB;

class AuthController extends Controller
{
    use ResponseTrait;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register','refresh']]);
    }

    /**
     * Register user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $success = false;
    	DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name_en' => 'required|string|min:2|max:100',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|string|confirmed|min:5',
            ]);

            if($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $user = User::create([
                    'username' => $request->name_en,
                    'name' => $request->name_en,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)
                ]);
            $role = Role::findByName('user');
            $user->syncRoles($role);
            $success = true;
            if ($success) {
                DB::commit();
            }
            return response()->json([
                'message' => 'User successfully registered',
                'user' => $user
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AuthPostRequest $request)
    {
        $validator = $request->validated();
        if (!$validator) {
           return $this->responseError(
                $validator,
                'Data is invalid',
                422
            );
        }

        if (!$token = auth()->attempt($validator)) {
            return $this->responseError(
                $validator,
                'Unauthorized',
                401
            );
        }
        return $this->responseUserSuccess(new LoginResource(['token'=>$this->respondWithToken($token),'refresh'=>$this->refresh()]), 200);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return $this->responseSuccess(['message' => 'User successfully logged out.']);
    }

    /**
     * Refresh token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        return $this->responseUserSuccess(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'accessToken' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
