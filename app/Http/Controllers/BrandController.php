<?php

namespace App\Http\Controllers;

use App\Helpers\UploadHelper;
use App\Http\Resources\MarqueResource;
use App\Http\Resources\UserResource;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BrandController extends Controller
{
    public function __construct(UploadHelper $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }


    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Brand::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('title', 'LIKE', '%' . $keyword . '%');
        }

        return MarqueResource::collection($userQuery->paginate($limit));
    }


    public function store(Request $request){
        try {
            $article= Brand::create([
                "title" => $request->title,
            ]);
            if($request->imagesArray){
                foreach($request->imagesArray as $file){
                    $article->image = $this->uploadFile::upload($file, 'brand');
                    $article->save();
                }
            }
            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    public function delete(Request $request)
    {
        try {
            $user=Brand::where('id', $request->marque)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Brand To Article create
     */
    public function getListBrand(){
        $list = Brand::all();
        return response()->json(['data'=>MarqueResource::collection($list) ,"code"=>200]);
    }
}
