<?php

namespace App\Http\Controllers;

use App\Http\Resources\BonCommandeResource;
use App\Models\BonCommande;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use PDF;
use File;

class BonCommandController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $bonCommandeQuery = BonCommande::query();
        $limit = Arr::get($searchParams, 'perPage', '');
        $keyword = $request->q;
        if (!empty($keyword)) {
            $bonCommandeQuery->Where('client_id', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('bonCommande_id', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('no_devis', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('montant_tht', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('montant_remise', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('net_ht', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('total_tva', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('timbre', 'LIKE', '%' . $keyword . '%');
            $bonCommandeQuery->orWhere('montant_ttc', 'LIKE', '%' . $keyword . '%');
        }
        return BonCommandeResource::collection($bonCommandeQuery->paginate($limit));
    }

    public function printInvoice($id)
    {
        $invoice = BonCommande::find($id);
        $pdf = PDF::loadView('devisTemplate', compact('invoice'));
        ini_set('max_execution_time', 300);
        $path = public_path().'/devis';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        $pdf->save('BonCommande/'.$invoice->no_devis.'.pdf');
        $invoice->file = asset('devis/'.$invoice->no_devis.'.pdf');
        $invoice->save();

        $headers = [
            'Content-Type' => 'application/pdf',
         ];

         return $pdf->output();

        // return $invoice->file;
       
    }

    public function getAllLivreurs(){
        $livreurs = User::role('livreur')->get();

        return response()->json(['data' => $livreurs, 'code' => 200]);

    }

    public function getInvoiceById($id)
    {
        $invoice = BonCommande::with('Devisarticles','client','getLivreur')->where('id',$id)->first();

        return $invoice;
    }

    public function last_bonCommande_id()
    {
        $last_invoice_id = BonCommande::all()->last();

        if($last_invoice_id)
        {
            return $last_invoice_id->id + 1;
        }else{
            return 0;
        }
    }


    public function store(Request $request)
    {
        try {
            $devis = BonCommande::create([
                "client_id" => $request->data['data']['client']['value'],
                "depot_id" => 1,
                "salesPerson" => $request->data['data']['salesPerson'],
                "note" => $request->data['data']['note'],
                "livreur" => $request->data['data']['livreur']['value'],
                "issuedDate" => date('Y-m-d', strtotime($request->data['data']['issuedDate'])),
                "dueDate" => date('Y-m-d', strtotime($request->data['data']['dueDate'])),
                "no_devis" => $request->data['data']['id'],


                "net_ht" => $request->data['data']['net_ht'],
                "total_tva" => $request->data['data']['total_tva'],
                "timbre" => $request->data['data']['timbre'],
                "montant_tht" => $request->data['data']['montant_tht'],
                "remise" => $request->data['data']['remise'],
                "montant_ttc" => $request->data['data']['final_total'],
                "livreur_ref" => $request->data['data']['livreur_ref'],
                "mnt_remise" => $request->data['data']['mnt_remise'],

            ]);

            foreach ($request->data['data']['items'] as $prod) {
                $prod_id_array[$prod['id']] = ['nbProduct' => $prod['qty'] ?? 1 , 'tva' => $prod['itemTitle']['tax'] ?? ''];
            }
            $devis->Devisarticles()->sync($prod_id_array, false);
            return response()->json(['data' => $devis, 'code' => 200]);

        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }
}
