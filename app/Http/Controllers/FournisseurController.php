<?php

namespace App\Http\Controllers;

use App\Http\Resources\FournisseurResource;
use App\Http\Resources\TvaResource;
use App\Models\Fournisseur;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FournisseurController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Fournisseur::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('type', 'LIKE', '%' . $keyword . '%');
        }

        return FournisseurResource::collection($userQuery->paginate($limit));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $tva = Fournisseur::create([
            'type' => $params['fournisseur']['ftype']['value'],
            'numero' => $params['fournisseur']['numero'],
            'nom_social' => $params['fournisseur']['nom_social'],
            'adresse' => $params['fournisseur']['adresse'],
            'num_tel' => $params['fournisseur']['num_tel'],
            'email' => $params['fournisseur']['email'],
            'num_fax' => $params['fournisseur']['num_fax'],
            'contact' => $params['fournisseur']['contact'],
            'matricule_fiscal' => $params['fournisseur']['matricule_fiscal'],
            'remise' => $params['fournisseur']['remise'],

        ]);
        return response()->json(['data' => $tva, 'code' => 200]);

    }

    public function delete(Request $request)
    {
        try {
            $user = Fournisseur::where('id', $request->fournisseur)->delete();
            return response()->json(['data' => 'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }
}
