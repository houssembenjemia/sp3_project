<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepotResource;
use App\Models\Depot;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DepotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $depotQuery = Depot::query();
        $limit = Arr::get($searchParams, 'perPage', '');
        $keyword = $request->q;
        if (!empty($keyword)) {
            $depotQuery->Where('name', 'LIKE', '%' . $keyword . '%');
            $depotQuery->orWhere('desc', 'LIKE', '%' . $keyword . '%');
        }
        return DepotResource::collection($depotQuery->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try {
            $inputs= $request->depot;
            Depot::create([
                'name'=>$inputs['name'],
                'desc'=>$inputs['desc'],
            ]);

            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Depot  $depot
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=>new DepotResource(Depot::find($id)),'code'=>200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Depot  $depot
     * @return \Illuminate\Http\Response
     */
    public function edit(Depot $depot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Depot  $depot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $data= $request->depot;

            $depot = Depot::find($data['id']);
            $depot->name = $data['name'];
            $depot->desc = $data['desc'];
            $depot->save();

            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Depot  $depot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Depot::where('id', $request->depot)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Depot To Devise create
     */
    public function getListDepot(){
        $list = Depot::all();
        return response()->json(['data'=>DepotResource::collection($list) ,"code"=>200]);
    }
}
