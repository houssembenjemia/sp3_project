<?php

namespace App\Http\Controllers;

use App\Http\Resources\TvaResource;
use App\Models\Tva;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class TvaController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Tva::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('name', 'LIKE', '%' . $keyword . '%');
        }

        return TvaResource::collection($userQuery->paginate($limit));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $tva = Tva::create([
            'name' => $params['tva']['name'],
            'taux' => $params['tva']['taux'],

        ]);
        return response()->json(['data' => $tva, 'code' => 200]);

    }

    public function delete(Request $request)
    {
        try {
            $user = Tva::where('id', $request->tva)->delete();
            return response()->json(['data' => 'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Tva To Article create
     */
    public function getListTva(){
        $list = Tva::all();
        return response()->json(['data'=>TvaResource::collection($list) ,"code"=>200]);
    }
}
