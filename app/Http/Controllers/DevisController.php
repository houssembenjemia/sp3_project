<?php

namespace App\Http\Controllers;

use App\Http\Resources\ArticleResource;
use App\Http\Resources\ClientResource;
use App\Http\Resources\DevisResource;
use App\Models\Article;
use App\Models\Client;
use App\Models\Devis;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use PDF;
use File;

class DevisController extends Controller
{
    public function printInvoice($id)
    {
        $invoice = Devis::find($id);
        $pdf = PDF::loadView('devisTemplate', compact('invoice'));
        ini_set('max_execution_time', 300);
        $path = public_path().'/devis';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        $pdf->save('devis/'.$invoice->no_devis.'.pdf');
        $invoice->file = asset('devis/'.$invoice->no_devis.'.pdf');
        $invoice->save();

        $headers = [
            'Content-Type' => 'application/pdf',
         ];

         return $pdf->output();

        // return $invoice->file;
       
    }

    public function getAllLivreurs(){
        $livreurs = User::role('livreur')->get();

        return response()->json(['data' => $livreurs, 'code' => 200]);

    }

    public function getInvoiceById($id)
    {
        $invoice = Devis::with('Devisarticles','client','getLivreur')->where('id',$id)->first();

        return $invoice;
    }

    public function last_invoice_id()
    {
        $last_invoice_id = Devis::all()->last();

        if($last_invoice_id)
        {
            return $last_invoice_id->id + 1;
        }else{
            return 0;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $devisQuery = Devis::query();
        $limit = Arr::get($searchParams, 'perPage', '');
        $keyword = $request->q;
        if (!empty($keyword)) {
            $devisQuery->Where('client_id', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('depot_id', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('no_devis', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('montant_tht', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('montant_remise', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('net_ht', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('total_tva', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('timbre', 'LIKE', '%' . $keyword . '%');
            $devisQuery->orWhere('montant_ttc', 'LIKE', '%' . $keyword . '%');
        }
        return DevisResource::collection($devisQuery->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $devis = Devis::create([
                "client_id" => $request->data['data']['client']['value'],
                "depot_id" => 1,
                "salesPerson" => $request->data['data']['salesPerson'],
                "note" => $request->data['data']['note'],
                "livreur" => $request->data['data']['livreur']['value'],
                "issuedDate" => date('Y-m-d', strtotime($request->data['data']['issuedDate'])),
                "dueDate" => date('Y-m-d', strtotime($request->data['data']['dueDate'])),
                "no_devis" => $request->data['data']['id'],


                "net_ht" => $request->data['data']['net_ht'],
                "total_tva" => $request->data['data']['total_tva'],
                "timbre" => $request->data['data']['timbre'],
                "montant_tht" => $request->data['data']['montant_tht'],
                "remise" => $request->data['data']['remise'],
                "montant_ttc" => $request->data['data']['final_total'],
                "livreur_ref" => $request->data['data']['livreur_ref'],
                "mnt_remise" => $request->data['data']['mnt_remise'],

            ]);

            foreach ($request->data['data']['items'] as $prod) {
                $prod_id_array[$prod['id']] = ['nbProduct' => $prod['qty'] ?? 1 , 'tva' => $prod['itemTitle']['tax'] ?? ''];
            }
            $devis->Devisarticles()->sync($prod_id_array, false);
            return response()->json(['data' => $devis, 'code' => 200]);

        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * getAllProductDevisClient.
     *
     * @param \App\Models\Famille $famille
     * @return \Illuminate\Http\Response
     */
    public function getAllProductDevisClient(Request $request)
    {
        $data = Article::where('id', $request->ProductIdList)->with('tva')->first();
        return response()->json(['data' => new ArticleResource($data), 'code' => 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Devis $devis
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data' => new DevisResource(Devis::where('id', $id)->with('Devisarticles')->first()), 'code' => 200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Devis $devis
     * @return \Illuminate\Http\Response
     */
    public function edit(Devis $devis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Devis $devis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $inputs = $request->data['data']['userData'];
            $devis = Devis::find($inputs['id']);
            $devis->client_id = $inputs['client_id'];
            $devis->depot_id = $inputs['depot_id'];
            $devis->no_devis = $inputs['no_devis'];
            $devis->montant_tht = $inputs['montant_tht'];
            $devis->montant_remise = $inputs['montant_remise'];
            $devis->net_ht = $inputs['net_ht'];
            $devis->total_tva = $inputs['total_tva'];
            $devis->timbre = $inputs['timbre'];
            $devis->montant_ttc = $inputs['montant_ttc'];
            $devis->save();

            $devis->Devisarticles()->detach();
            // $devis->Devisarticles()->sync(array_column($inputs['Devisarticles'],'id'));
            foreach ($inputs['Devisarticles'] as $prod) {
                $prod_id_array[$prod['id']] = ['nbProduct' => $prod['nbProduct'] ?? 1];
            }
            $devis->Devisarticles()->sync($prod_id_array, false);
            return response()->json(['message' => 'success', "code" => 200]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Devis $devis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Devis::where('id', $request->user)->delete();
            return response()->json(['data' => 'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Famille To Article create
     */
    public function getListClient()
    {
        $list = Client::all();
        return response()->json(['data' => ClientResource::collection($list), "code" => 200]);
    }

    public function getDevistPdf($id)
    {
        $devis = Devis::find($id);
        $pdf = PDF::loadView('devisTemplate', compact('devis'));

        return $pdf->download("s3p.pdf");

    }
}
