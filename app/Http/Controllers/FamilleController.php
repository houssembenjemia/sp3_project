<?php

namespace App\Http\Controllers;

use App\Helpers\UploadHelper;
use App\Http\Resources\FamilleResource;
use App\Models\Famille;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FamilleController extends Controller
{
    public function __construct(UploadHelper $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Famille::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('title', 'LIKE', '%' . $keyword . '%');
            $userQuery->orWhere('code_famille', 'LIKE', '%' .$keyword . '%');
            $userQuery->orWhere('design', 'LIKE', '%' .$keyword . '%');
            $userQuery->orWhere('image', 'LIKE', '%' .$keyword . '%');
            $userQuery->orWhere('famille_parentale', 'LIKE', '%' .$keyword . '%');
            $userQuery->orWhere('default_tva', 'LIKE', '%' .$keyword . '%');
            $userQuery->orWhere('default_marge', 'LIKE', '%' .$keyword . '%');
        }

        return FamilleResource::collection($userQuery->paginate($limit));
    }

    /**
     * List Famille To Article create
     */
    public function getListFamille(){
        $list = Famille::all();
        return response()->json(['data'=>FamilleResource::collection($list) ,"code"=>200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try {
            Famille::create([
                'code_famille'=>$request->code_famille,
                'design'=>$request->design,
                'image'=>$this->uploadFile::upload($request->image, 'familles'),
                'famille_parentale'=>$request->famille_parentale,
                'default_tva'=>$request->default_tva,
                'default_marge'=>$request->default_marge,
            ]);

            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=>new FamilleResource(Famille::find($id)),'code'=>200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function edit(Famille $famille)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            Famille::where('id',$request->id)->update([
                'code_famille'=>$request->code_famille,
                'design'=>$request->design,
                'image'=>$request->image?$this->uploadFile::upload($request->image, 'familles'):Famille::find($request->id)->pluck('image')[0],
                'famille_parentale'=>$request->famille_parentale,
                'default_tva'=>$request->default_tva,
                'default_marge'=>$request->default_marge,
             ]);

            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Famille::where('id', $request->user)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }
}
