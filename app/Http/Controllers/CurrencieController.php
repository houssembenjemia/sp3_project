<?php

namespace App\Http\Controllers;

use App\Http\Resources\CurrencieResource;
use App\Http\Resources\UnitResource;
use App\Models\Currencie;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CurrencieController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Currencie::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('title', 'LIKE', '%' . $keyword . '%');
        }

        return CurrencieResource::collection($userQuery->paginate($limit));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $tva = Currencie::create([
            'name' => $params['currencie']['name'],
            'code' => $params['currencie']['code'],
            'valeur_echange' => $params['currencie']['valeur_echange'],

        ]);
        return response()->json(['data' => $tva, 'code' => 200]);

    }

    public function delete(Request $request)
    {
        try {
            $user=Currencie::where('id', $request->currencie)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }
}
