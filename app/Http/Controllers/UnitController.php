<?php

namespace App\Http\Controllers;

use App\Http\Resources\MarqueResource;
use App\Http\Resources\UnitResource;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UnitController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Unit::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->Where('title', 'LIKE', '%' . $keyword . '%');
        }

        return UnitResource::collection($userQuery->paginate($limit));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $tva = Unit::create([
            'title' => $params['unit']['title'],
            'code' => $params['unit']['code'],
            'unite_de_base' => $params['unit']['unite_de_base'],

        ]);
        return response()->json(['data' => $tva, 'code' => 200]);

    }

    public function delete(Request $request)
    {
        try {
            $user=Unit::where('id', $request->unit)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Unite To Article create
     */
    public function getListUnite(){
        $list = Unit::all();
        return response()->json(['data'=>UnitResource::collection($list) ,"code"=>200]);
    }
}
