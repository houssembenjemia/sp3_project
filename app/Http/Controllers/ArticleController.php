<?php

namespace App\Http\Controllers;

use App\Helpers\UploadHelper;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Models\ArticleFile;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ArticleController extends Controller
{
    public function __construct(UploadHelper $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchParams = $request->params;
        $userQuery = Article::query();
        // $userQuery->whereHas('roles', function($q) { $q->where('name', 'admin'); });
        $limit = Arr::get($searchParams, 'perPage', '');

        $keyword = $request->q;

        if (!empty($keyword)) {
            $userQuery->where('design', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('code_bar', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('ref', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('libele_cataloge', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('id3', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('id2', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('id1', 'LIKE', '%', $keyword, '%');
            // $userQuery->orWhere('tva', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('achat_ttc', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('prix_achat_ht', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('pv_ttc', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('prix_vente', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('marge', 'LIKE', '%', $keyword, '%');
            // $userQuery->orWhere('marque', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('max_marge', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('matiere_active', 'LIKE', '%', $keyword, '%');
            // $userQuery->orWhere('unite_prod', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('qty_alert', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('remise', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('deb_remise', 'LIKE', '%', $keyword, '%');
            $userQuery->orWhere('fin_remise', 'LIKE', '%', $keyword, '%');
        }

        return ArticleResource::collection($userQuery->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadCsv(Request $request)
    {
        dd($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try {
           $article= Article::create([
                "famille_id" => $request->famille_id,
                "design" => $request->design,
                "code_bar" => $request->code_bar,
                "ref" => $request->ref,
                "libele_cataloge" => $request->libele_cataloge,
                "id4" => $request->id4,
                "id3" => $request->id3,
                "id2" => $request->id2,
                "id1" => $request->id1,
                "tva_id" => $request->tva_id,
                "achat_ttc" => $request->achat_ttc,
                "prix_achat_ht" => $request->prix_achat_ht,
                "pv_ttc" => $request->pv_ttc,
                "prix_vente" => $request->prix_vente,
                "marge" => $request->marge,
                "brand_id" => $request->brand_id,
                "max_marge" => $request->max_marge,
                "matiere_active" => $request->matiere_active,
                "unit_id" => $request->unit_id,
                "qty_alert" => $request->qty_alert,
                "remise" => $request->remise,
                "deb_remise" => $request->deb_remise,
                "fin_remise" => $request->fin_remise,
            ]);
            if($request->hasFile('imagesArray')){
                foreach($request->imagesArray as $file){
                    $f = new ArticleFile(); $f->url = $this->uploadFile::upload($file, 'articles');
                    $f->article_id=$article->id;
                    $f->save();
                }
            }
            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=>new ArticleResource(Article::find($id)->with('files')->first()),'code'=>200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $article= Article::where('id',$request->id)->update([
                "famille_id" => $request->famille_id,
                "design" => $request->design,
                "code_bar" => $request->code_bar,
                "ref" => $request->ref,
                "libele_cataloge" => $request->libele_cataloge,
                "id3" => $request->id3,
                "id4" => $request->id4,
                "id2" => $request->id2,
                "id1" => $request->id1,
                "tva_id" => $request->tva_id,
                "achat_ttc" => $request->achat_ttc,
                "prix_achat_ht" => $request->prix_achat_ht,
                "pv_ttc" => $request->pv_ttc,
                "prix_vente" => $request->prix_vente,
                "marge" => $request->marge,
                "brand_id" => $request->brand_id,
                "max_marge" => $request->max_marge,
                "matiere_active" => $request->matiere_active,
                "unit_id" => $request->unit_id,
                "qty_alert" => $request->qty_alert,
                "remise" => $request->remise,
                "deb_remise" => $request->deb_remise,
                "fin_remise" => $request->fin_remise,
             ]);
             if($request->hasFile('imagesArray')){
                 foreach($request->imagesArray as $file){
                    $f = new ArticleFile(); $f->url = $this->uploadFile::upload($file, 'articles');
                    $f->article_id=$article;
                    $f->save();
                }
            }
            return response()->json(['message'=>'success', "code"=>200]);
        }catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Article::where('id', $request->user)->delete();
            return response()->json(['data'=>'success', 201]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * List Article To Devis create
     */
    public function getListArticle(){
        $list = Article::all();
        return response()->json(['data'=>ArticleResource::collection($list) ,"code"=>200]);
    }
}
