<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class AuthPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'name_en' => 'required|string|min:2|max:100',
            'email' => 'required|string|email|max:100',
            'password' => 'required|string|min:5',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'name_en' => 'Name EN is required',
            'email' => 'Email is required for login',
            'password' =>'Password is required for login'
        ];
    }
}
