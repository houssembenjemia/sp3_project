<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable=[
        'famille_id',
        'design',
        'code_bar',
        'ref',
        'libele_cataloge',
        'id4',
        'id3',
        'id2',
        'id1',
        'tva_id',
        'achat_ttc',
        'prix_achat_ht',
        'pv_ttc',
        'prix_vente',
        'marge',
        'brand_id',
        'max_marge',
        'matiere_active',
        'unit_id',
        'qty_alert',
        'remise',
        'deb_remise',
        'fin_remise',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function Unite()
    {
        return $this->belongsTo(Unit::class);
    }

    public function tva()
    {
        return $this->belongsTo(Tva::class);
    }

    public function famille()
    {
        return $this->belongsTo(Famille::class);
    }

    public function files()
    {
        return $this->hasMany(ArticleFile::class,'article_id');
    }
}
