<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'num_client',
        'nom_social',
        'adresse',
        'num_tel',
        'num_fax',
        'email',
        'contact',
        'num_portable',
        'num_id_fiscale',
        'exonorer',
        'timbre',
        'remise',
        'mnt_garanti',
        'piece_garanti',
        'activiter',
        'zone',
        'categorie',
        'num_chef_groupe',
        'libelle_chef_groupe',
    ];
}
