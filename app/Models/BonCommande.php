<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonCommande extends Model
{
    use HasFactory;

    protected $fillable=[
        'client_id',
        'depot_id',
        // 'montant_tht',
        // 'montant_remise',
        'montant_ttc',
        // 'no_devis',
        // 'net_ht',
        // 'total_tva',
        // 'timbre'
        'discount',
        'tax1',
        'tax2',
        'salesPerson',
        'note',
        'issuedDate',
        'dueDate',
        'livreur',
        'no_devis',
        'net_ht',

        'total_tva',
        'timbre',
        'montant_tht',
        'remise',
        'livreur_ref',
        'mnt_remise'
    ];
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function depot()
    {
        return $this->belongsTo(Depot::class);
    }

    public function Devisarticles()
    {
        return $this->belongsToMany(Article::class,'bon_commande_devis','devis_id','article_id')->withPivot('nbProduct');
    }

    public function getLivreur()
    {
        return $this->belongsTo(User::class,'livreur','id');
    }
}
