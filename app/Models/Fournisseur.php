<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'numero',
        'image',
        'nom_social',
        'adresse',
        'num_tel',
        'email',
        'num_fax',
        'contact',
        'matricule_fiscal',
        'remise',
        'id',
    ];
}
