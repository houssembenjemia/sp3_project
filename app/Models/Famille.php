<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Famille extends Model
{
    use HasFactory;
    protected $fillable= [
        'code_famille',
        'design',
        'image',
        'famille_parentale',
        'default_tva',
        'default_marge',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
