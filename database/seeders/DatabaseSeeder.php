<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use App\Models\User;
use App\Models\Branch;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role= Role::create(['name'=>'admin','guard_name' => 'api']);
        Role::create(['name'=>'user','guard_name' => 'api']);
        $user= User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('147258'),
        ]);
        $user->assignRole($role);

        $this->call([
                //
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
