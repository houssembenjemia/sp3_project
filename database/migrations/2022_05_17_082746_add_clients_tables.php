<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('num_client');
            $table->string('nom_social')->nullable();
            $table->text('adresse')->nullable();
            $table->string('num_tel')->nullable();
            $table->string('num_fax')->nullable();
            $table->string('email')->nullable();
            $table->text('contact')->nullable();
            $table->string('num_portable')->nullable();
            $table->string('num_id_fiscale')->nullable();
            $table->string('exonorer')->nullable();
            $table->string('timbre')->nullable();
            $table->string('remise')->nullable();
            $table->string('mnt_garanti')->nullable();
            $table->string('piece_garanti')->nullable();
            $table->string('activiter')->nullable();
            $table->string('zone')->nullable();
            $table->string('categorie')->nullable();
            $table->string('num_chef_groupe')->nullable();
            $table->string('libelle_chef_groupe')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
