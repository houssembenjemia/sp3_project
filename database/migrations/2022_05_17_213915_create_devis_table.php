<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->nullable()->constrained();
            $table->foreignId('depot_id')->constrained();
            // $table->longText('no_devis');
            // $table->double('montant_tht');
            // $table->double('montant_remise');
            // $table->double('net_ht');
            // $table->double('total_tva');
            // $table->double('timbre');
            // $table->double('montant_ttc');
            $table->string('discount');
            $table->string('tax1');
            $table->string('tax2');
            $table->string('salesPerson');
            $table->string('note');
            $table->date('issuedDate');
            $table->date('dueDate');
            $table->string('livreur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis');
    }
}
