<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BonCommande extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bon_commandes', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('depot_id');
            $table->string('montant_ttc')->nullable();
            $table->string('discount')->nullable();
            $table->string('tax1')->nullable();
            $table->string('tax2')->nullable();
            $table->string('salesPerson')->nullable();
            $table->string('note')->nullable();
            $table->string('issuedDate')->nullable();
            $table->string('dueDate')->nullable();
            $table->string('livreur')->nullable();
            $table->string('no_devis')->nullable();
            $table->string('net_ht')->nullable();

            $table->string('total_tva')->nullable();
            $table->string('timbre')->nullable();
            $table->string('montant_tht')->nullable();
            $table->string('remise')->nullable();
            $table->string('livreur_ref')->nullable();
            $table->string('mnt_remise')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
