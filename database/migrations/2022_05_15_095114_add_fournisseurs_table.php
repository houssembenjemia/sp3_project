<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFournisseursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fournisseurs', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('numero');
            $table->longText('image')->nullable();
            $table->string('nom_social');
            $table->string('adresse');
            $table->string('num_tel');
            $table->string('email');
            $table->string('num_fax');
            $table->string('contact');
            $table->string('matricule_fiscal');
            $table->string('remise');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
