<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('famille_id')->constrained();
            $table->longText('design')->nullable();
            $table->longText('code_bar')->nullable();
            $table->longText('ref')->nullable();
            $table->longText('libele_cataloge')->nullable();
            $table->longText('id3')->nullable();
            $table->longText('id4')->nullable();
            $table->longText('id2')->nullable();
            $table->longText('id1')->nullable();
            $table->double('achat_ttc')->nullable();
            $table->double('prix_achat_ht')->nullable();
            $table->double('pv_ttc')->nullable();
            $table->double('prix_vente')->nullable();
            $table->longText('marge')->nullable();
            $table->double('max_marge')->nullable();
            $table->longText('matiere_active')->nullable();
            $table->double('qty_alert')->nullable();
            $table->double('remise')->nullable();
            $table->date('deb_remise')->nullable();
            $table->date('fin_remise')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
